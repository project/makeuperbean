MakeUp ER Bean
==============

Maintainer: w3wfr (<drupal@w3w.fr>)

MakeUp ER Bean provides a formatter that uses View mode field value from Bean
content when attached from an Entity Reference field.

### Features

Bean module provides a View mode field that is used to define which view mode
should be considered while displaying a Bean content as a Block.

When attaching a Bean to a Node (or any other entity of contents) through an
Entity Reference field: the view mode is statically defined at view mode level
- as for any other Entity.

This module provide a formatter that use View mode defined at content level
(referencee) instead of using the one defined at View mode level (referencer).

Notice: If used on different bundles through out a website (including from
different ER Base fields), a given Bean content will be similarly displayed
throughout the website.

Requirements
------------

 * Drupal 7.x

### Modules

#### Depedency

 * Field
 * Entity Reference
 * Bean

Installation
------------

Ordinary installation.
[http://drupal.org/documentation/install/modules-themes/modules-7]
(http://drupal.org/documentation/install/modules-themes/modules-7)
